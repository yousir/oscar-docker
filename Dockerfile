FROM debian:bullseye-slim
ADD https://www.apneaboard.com/OSCAR/oscar_1.3.0-Debian11_amd64.deb /tmp/oscar.deb
RUN apt update && apt -y upgrade && apt -y install -f /tmp/oscar.deb