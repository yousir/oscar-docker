# Docker image containing OSCAR

OSCAR, the Open Source CPAP Analysis Reporter

> OSCAR is PC software developed for reviewing and exploring data produced by CPAP and related machines used in the treatment of sleep apnea.
> -- https://www.sleepfiles.com/OSCAR/

# Usage

    git clone https://gitlab.com/yousir/oscar-docker
    cd oscar-docker
    docker-compose up

# Config

Edit `OSCAR.conf` if you want to change data dir, language and such.

The data dir is set to `/OSCAR_Data` and the language is set to the default `en_US`.

Currently there's a bug so you cannot change the language in the GUI or it'll crash. And for some reason, even though you're prompted to choose language when starting the application, the choice doesn't get saved in the config file. This makes the prompt pop up every time the application starts, not just the first, because the config file doesn't have a default language already set. To prevent this, the default language is specified in the config beforehand, and instead of changing language in the GUI, just change it in `OSCAR.conf`.

These are the languages that are currently available:

    af	ar	bg	da	de	el	en_UK	es	es_MX	fi
    fr	he	hu	it	ko	nl	no	ph	pl	pt	
    pt_BR	ro	ru	sv	th	tr	zh_CN	zh_TW

If you change data dir you should change mount points in `docker-compose.yml` as well.

Also beware that you have to create a new profile before you can change any preferences from the GUI, because this will also otherwise result in a crash.

If you do any changes to `docker-compose.yml` or `Dockerfile` you'll have to rebuild it by running

    docker-compose up --build

# Links
- https://www.sleepfiles.com/OSCAR/
- https://gitlab.com/pholy/OSCAR-code
- https://gitlab.com/CPAPreporter/oscar-code
- http://www.apneaboard.com/wiki/index.php/OSCAR_Help
